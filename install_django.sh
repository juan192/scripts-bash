#!/bin/bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-pip
sudo pip install virtualenv
mkdir ~/.virtualenvs
sudo pip install virtualenvwrapper
export WORKON_HOME=~/.virtualenvs
echo ". /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
. ~/.bashrc
